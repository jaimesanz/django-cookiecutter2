# How to use this

Install cookiecuter on your machine:

`pip install cookiecutter`

then run 

`cookiecutter git@gitlab.com:jaimesanz/django-cookiecutter2.git`

you'll be prompted to enter the project's name

# References

* https://cookiecutter.readthedocs.io/en/latest/first_steps.html#your-first-cookiecutter